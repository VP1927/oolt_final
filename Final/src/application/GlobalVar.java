package application;

public class GlobalVar {
	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 600;
	public static final int PRIMARY_X = 0;
	public static final int PRIMARY_Y = 0;
	
	public static final int LABEL_WIDTH = 75;
	public static final int LABEL_HEIGHT = 50;
	
}
